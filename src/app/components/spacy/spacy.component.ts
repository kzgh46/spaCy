import { Component, OnInit } from '@angular/core';
import { Spacy } from 'src/app/models/spacy';
import { SpacyService } from 'src/app/services/spacy.service';

@Component({
  selector: 'app-spacy',
  templateUrl: './spacy.component.html',
  styleUrls: ['./spacy.component.css']
})
export class SpacyComponent implements OnInit {

  public spacy: Spacy = {input: "", output:"", model: "en"};

  constructor(private _spacyService: SpacyService) { }

  ngOnInit(): void {
  }

  getResponse(): void{
    this._spacyService.getResponse(this.spacy.input, this.spacy.model).subscribe(
      // data => this.spacy.output = data
      data => this.spacy.output = data
     );
  }


}
