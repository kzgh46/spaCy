import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SpacyService {
  private _url: string = `https://spacy.frag.jetzt/dep`;

  constructor(private http: HttpClient) {}

  getResponse(input: string, model: string): Observable<string> {
    return this.http.post<string>(this._url, {
      text: input,
      model: model
    });
  }
}
