export interface Spacy{
  input: string;
  output: string;
  model: string;
}
